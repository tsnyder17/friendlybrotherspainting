import Layout from '../components/layout';

export default function About() {
  return (
    <Layout>
      <div id="wrapper">
        <p>"My company, Friendly Brothers Painting, has been in business since 1985 and we are committed to providing a high quality, finished product to every residential and commercial client that contracts us for painting. Our loyalty and commitment to your satisfaction is our main goal to every indivual client. We strive for your experience to be a relaxed yet exciting new change for your business or family. Friendly Brothers offers our clients superior quality workmanship utilizing long lasting paint products to create the best finished product possible". <br /> -- Mark Keider <br /> Owner <br /> Friendly Brothers Painting</p>
      </div>
  <style jsx>{`
    #wrapper {
      // margin-top: 200px;
    }
  `}</style>
    </Layout>
  );
}