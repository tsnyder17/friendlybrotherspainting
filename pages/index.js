import Layout from '../components/layout';
import { motion } from 'framer-motion';

export default function Index() {
  return (
    <Layout>
      <div id="hero-img">
        <div id="hero-text-wrap-parent">
            <div id="hero-text-wrap">
            <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }}
            transition={{ duration: 1 }}>
              <div id="hero-text">
                Friendly Brothers Painting <br />
                <i><p id="hero-subtext">Professional Painting Since 1985.</p></i>
              </div>
            {/* <div id="hero-text-wrap-shadow">
            </div> */}
            </motion.div>
            </div>
        </div>
      </div>
      <style jsx>{
        `
          #hero-img {
            height: 100vh;
            background-image: url(/white-wooden-cupboards-2724749-min.jpg);
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;
          }

          #hero-text-wrap-parent {
            position: absolute;
            top: 35vh;
            width: 100%;
            text-align: center;
          }

          #hero-text-wrap {
            position: relative;
            text-align: center;
            width: -moz-fit-content;
            width: fit-content;
            margin: 0 auto;
          }

          #hero-text-wrap:before {
            backdrop-filter: blur(10px);
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
          }

          #hero-text-wrap:after {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            box-shadow: 0px 5px 23px 0px rgba(0,0,0,0.75);
            border-radius: 2px;
          }

          #hero-text {
            position: relative;
            margin: auto;
            padding: 1em 2em;
            font-family: auto;
            font-size: -webkit-xxx-large;
            color: white;
            text-shadow: 1px 1px #000000;
          }

          #hero-subtext {
            font-size: x-large;
            margin: 2%;
          }
        `
      }
      </style>
    </Layout>
  );
}