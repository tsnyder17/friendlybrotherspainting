import Layout from '../components/layout';
import Head from 'next/head'

export default function Contact() {
  return (
    <Layout>
      <div id="wrapper">
        <div id="left">
          <div id="form-wrap-parent">
            <div id="form-wrap">
              <div id="form-content">
                <h2>Friendly Brothers Painting</h2>
                <h3>To schedule your free estimate, you can call us at 734-707-5887, email us at test@testemail.com, or fill out the form below.</h3>
                <h3>We look forward to working with you!</h3>
                <form id="contact-form">
                  <div className="flex-container">
                    <input placeholder="First Name" className="input half"></input>
                    <input placeholder="Last Name" className="input half"></input>
                  </div>
                  {/* <div className="flex-container"> */}
                    <input placeholder="Phone Number" className="input "></input>
                    <input placeholder="Email Address" className="input "></input>
                  {/* </div> */}
                  <textarea placeholder="Your Exciting Ideas" className="input"></textarea>
                </form>
                <button id="submit-btn" className="button">Send</button>
              </div>
            </div>
          </div>
        </div>
        <div id="right"></div>
      </div>
  <style jsx>{`
    #wrapper {
      display: flex;
      height: 100vh;
      background-image: url(/rollershot-min.png);
      background-position: center center;
      background-repeat: no-repeat;
      background-size: cover;
    }

    #right {
      width: 0%;
      height: 100%;
      display: none
    }

    #left {
      position: relative;
      width: 65%;
      height: 100%;
    }

    #form-wrap-parent {
      position: absolute;
      top: 15vh; // 15vh;
      width: 100%;
      text-align: center;
    }

    #form-wrap {
      position: relative;
      width: 75%;
      margin: 0 auto;
      color: white;
      padding: 1em;
    }

    #form-wrap:before {
      backdrop-filter: blur(10px);
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
    }

    #form-wrap:after {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      box-shadow: 0px 5px 23px 0px rgba(0,0,0,0.75);
      border-radius: 2px;
    }

    #form-content {
      position: relative;
      width: 80%;
      margin: 0 auto;
      color: white;
      padding: 1em;
      z-index: 1;
    }

    .flex-container {
      display: flex;
    }

    h2 {
      font-size: xx-large;
      font-weight: 300;
      text-shadow: 1px 1px #000000;
    }

    h3 {
      margin: 0;
      font-size: larger;
      font-weight: 300;
      color: rgba(0,0,0,.65);
    }

    #contact-form {
      padding: 1em;
      width: fit-content;
      margin: auto;
    }

    .input {
      padding: 0.75em;
      border-radius: 5px;
      border: 1px solid lightgrey;
      font-size: 14px;
      outline: none;
      line-height: 1.6;
      width: 100%;
      margin: .25em;
      box-sizing:border-box
      font-family: inherit;
    }

    .input:hover, input:focus {
      box-shadow: 5px 5px 5px rgba(0,0,0,0.15);
    }

    #submit-btn {
      background: #377dff;
      padding: 1% 1em;
      border-radius: 5px;
      font-size: inherit;
      color: white;
      height: fit-content;
      margin: auto;
      height: 3em;
      width: 50%;
    }

    .button {
      border: none;
      display: inline-block;
      cursor: pointer;
      transition: all ease 200ms;
      font-size: initial !important;

    }
    .button:hover {
      box-shadow: 0 35px 20px -20px rgba(0, 0, 0, 0.3);
      transform: translate(0px, -1px);
    }

    .half {
      width: 50%;
    }

    textarea {
      margin: 0.5em;
      font-family: sans-serif;
    }
  `}</style>
    </Layout>
  );
}