import Link from 'next/link';

const linkStyle = {
  marginRight: 15,
  fontSize: '1.35em'
};

const navBar = () => {
  return (
      <div id="navbar">
        <div id="logo">
          <Link href="/">
            <a>index</a>
          </Link>
        </div>
        <div id="nav-list">
          <ul>
            <li>
              <Link href="/about">
                <a title="About Page" style={linkStyle}>About</a>
              </Link>
            </li>
            <li>
              <Link href="/">
                <a style={linkStyle}>Past Projects</a>
              </Link>
            </li>
            <li>
              <Link href="/contact">
                <a style={linkStyle}>Contact</a>
              </Link>
            </li>
          </ul>
        </div>
        <div id="login-btn">
          <Link href="/">
            <button style={linkStyle} id="login" className="button">Get an estimate</button>
          </Link>
        </div>
        <style jsx global>{`


            body {
              margin: 0;
            }

          `}</style>
        <style jsx>{`
        #logo {
          height: 2.25em;
          margin: auto 0;
        }

        #logo:hover {
          cursor: pointer;
        }

        #navbar {
          background-color: rgba(255,255,255,0.675);
          display: flex;
          position: absolute;
          top: 0;
          width: 100%;
          overflow: hidden;
          z-index: 999;
          padding: 0.25em 0;
          height: 3em;
        }

        #logo, #nav-list, #login-btn {
          width: 33.3334%;
          height: 100%;
          overflow: visible;
        }

        #nav-list {
          text-align: center;
        }

        #login-btn {
          text-align: right;
        }

        ul {
          display: flex;
          list-style: none;
          margin: 0 auto;
          padding: 0;
          height: 100%;
        }

        li {
          padding-right: 1.5rem;
          padding-left: 1.5rem;
          font-weight: 600;
          margin: auto;
        }

        #login {
          background: #377dff;
          padding: 1% 1em;
          border-radius: 5px;
          font-size: inherit;
          color: white;
          height: fit-content;
          margin: auto;
          height: 100%;
        }

        a {
          text-decoration: none;
          color: rgba(0,0,0,.65);
          font-weight: lighter;
        }

        a:hover {
          color: rgba(0,0,0,.5);;
          cursor: pointer;
          text-shadow: none;
        }

        .button {
          border: none;
          display: inline-block;
          cursor: pointer;
          transition: all ease 200ms;
          font-size: initial !important;

        }
        .button:hover {
          box-shadow: 0 35px 20px -20px rgba(0, 0, 0, 0.3);
          transform: translate(0px, -1px);
        }

        .arrowDown {
          vertical-align: middle;
        }
        `
        }
        </style>
      </div>
  );
}
export default navBar
