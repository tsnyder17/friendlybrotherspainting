import Link from 'next/link';

const footer = () => {
    return (
        <div id="footer">
            <div id="footer-text">
                <h2>Friendly Brothers Painting LLC.</h2>
                <p>Phone: 734-555-5555 | Email: test@gmail.com <br /> Copyright &copy; Friendly Brothers Painting LLC.</p>
            </div>

            <style jsx>{`
                #footer {
                    background: dimgrey;
                    padding: 1em;
                }

                #footer-text {
                    font-weight: lighter;
                    text-align: center;
                    color: lightgrey;
                    font-weight: lighter;
                    text-shadow: 1px 1px #000000;
                }

                h2 {
                    font-size: x-large;
                }
            `}
            </style>
        </div>
    )
}

export default footer;
