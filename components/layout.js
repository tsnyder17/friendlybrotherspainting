import Header from './header';
import Footer from './footer';

const Layout = props => (
  <div >
    <Header />
    <div id="page-container">
	    <div id="content-wrap">
        {props.children}
      </div>
    </div>
    <Footer />
    <style jsx>
      {`
        #page-container {
          position: relative;
          min-height: 100vh;
        }
      `}
    </style>
  </div>
);

export default Layout;